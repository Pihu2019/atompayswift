
//  ProdModel.h
//  AtomPayLib
//  Created by Work on 16/11/16.
//  Copyright © 2016 ATOM TECHNOLOGIES. All rights reserved.

#ifndef ProdModel_h
#define ProdModel_h


#endif /* ProdModel_h */

@interface ProdModel : NSObject

@property (strong, nonatomic) NSString *pid;
@property (strong, nonatomic) NSString *name;
@property (strong, nonatomic) NSString *amount;
@property (strong, nonatomic) NSString *param1;
@property (strong, nonatomic) NSString *param2;
@property (strong, nonatomic) NSString *param3;
@property (strong, nonatomic) NSString *param4;
@property (strong, nonatomic) NSString *param5;

@end
