//
//  RnDViewController.m
//  WebView
//
//  Created by ATOM TECHNOLOGIES on 10/15/13.
//  Copyright (c) 2013 ATOM TECHNOLOGIES. All rights reserved.

#import "nb.h"
#import "Reachability.h"
#import <CommonCrypto/CommonHMAC.h>

@interface nb ()<UIWebViewDelegate>
{
    NSXMLParser *parser;
    NSString *element;
    
    NSDictionary *myAttrDict;
    NSString *attributesNames;
    NSString *attributesNamesId;
    NSMutableArray *names;
    
    NSMutableArray *formParam;
    NSMutableArray *formUrl;
    NSString *result;
    
}
@end

@implementation nb

@synthesize activityIndicator,viewWeb,imageView,codeviewWeb,myDelegate,
merchantId,txnscamt,loginid,password,prodid,txncurr,clientcode,custacc,amt,txnid,date,bankid,ru,waitLabel,prods,reqHashKey,resHashKey;

- (void)viewDidLoad
{
    activityIndicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    
    [activityIndicator startAnimating];
    
    
    
    float h =self.view.bounds.size.height;
    float w = self.view.bounds.size.width;
    if(h > 480)
    {
        activityIndicator.frame = CGRectMake(w/2-50,h/2-100,100,100);
        viewWeb = [[UIWebView alloc]initWithFrame:CGRectMake(0, 64, w, h-64)];
    }else
    {
        activityIndicator.frame = CGRectMake(w/2-50,h/2-100,80,40);
        viewWeb = [[UIWebView alloc]initWithFrame:CGRectMake(0, 64, w, h-64)];
    }
    
    
    UIView *topView = [[UIView alloc] initWithFrame:CGRectMake(0.0f, 0.0f, w, 64.0f)];
    [topView setBackgroundColor:[UIColor grayColor]];
    
    
    UIButton *cancelButton =[UIButton buttonWithType:UIButtonTypeCustom];
    [cancelButton setTitle:@"Cancel" forState:UIControlStateNormal];
    [cancelButton setFrame:CGRectMake(w-80, 12.0f, 72.0f, 48.0f)];
    [cancelButton addTarget:self action:@selector(cancelNav:) forControlEvents:UIControlEventTouchUpInside];
    
    [topView addSubview:cancelButton];
    
    
    [self.view  addSubview:topView];
    
    
    viewWeb.scrollView.scrollEnabled = TRUE;
    
    [self.viewWeb addSubview:activityIndicator];
    
    //CGFloat labelX = activityIndicator.bounds.size.width + 2;
    
    waitLabel = [[UILabel alloc] initWithFrame:CGRectMake(w/2-100, h/2-100, 200, 50)];
    waitLabel.autoresizingMask = UIViewAutoresizingFlexibleWidth;
    waitLabel.font = [UIFont boldSystemFontOfSize:12.0f];
    waitLabel.numberOfLines = 2;
    
    waitLabel.backgroundColor = [UIColor clearColor];
    waitLabel.textColor = [UIColor blackColor];
    waitLabel.text = @"Please wait, while we processing your request..";
    
    [self.viewWeb addSubview:waitLabel];

    
    UIImage *image = [UIImage imageNamed:@"atom-logo.png"];
    
    imageView = [[UIImageView alloc] initWithImage:image];
    [imageView setFrame:CGRectMake(ceil(CGRectGetWidth(viewWeb.frame) - 140), h-144, 126, 66)];
    
    imageView.autoresizingMask = UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleTopMargin;
    
    // [imageView addSubview:activityIndicator];
    [self.viewWeb setDelegate:self];
    [self.viewWeb addSubview:imageView];
    [self.view  addSubview:self.viewWeb];
    // [imageView addSubview:label];
    
    [self initialCall];
    // [activityIndicator stopAnimating];
}

- (void)viewDidAppear:(BOOL)animated{
     NSLog(@"viewDidAppear");
    [super viewDidAppear:animated];
    
    
    [activityIndicator removeFromSuperview];
    [activityIndicator stopAnimating];
    [waitLabel removeFromSuperview];
    
    
    //       Reachability *networkReachability = [Reachability reachabilityForInternetConnection];
    //       NetworkStatus networkStatus = [networkReachability currentReachabilityStatus];
    //        if (networkStatus == NotReachable) {
    //          //  NSLog(@"NO internet connection");
    //            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Network Not Available"
    //                                                            message:@"Please check the Network connectivity "
    //                                                           delegate:self
    //                                                  cancelButtonTitle:@"OK"
    //                                                  otherButtonTitles: nil];
    //            [alert show];
    //
    //        } else {
    
    
    //   }
    
}

-(void)initialCall
{
    //ru = @"https://payment.atomtech.in/mobilesdk/param";
    
    if(merchantId == nil || [merchantId isEqualToString:@""]){
        merchantId = loginid;
    }
    ru=@"https://paynetzuat.atomtech.in/mobilesdk/param";
    
    NSString *urlstring = [NSString stringWithFormat:@"https://paynetzuat.atomtech.in/paynetz/epi/fts?login=%@&pass=%@&ttype=NBFundTransfer&prodid=%@&amt=%@&txncurr=%@&txnscamt=%@&clientcode=%@&txnid=%@&date=%@&custacc=%@&ru=%@",loginid,password,prodid,amt,txncurr,txnscamt,clientcode,txnid,date,custacc,ru];
    
    if(bankid != NULL && !([bankid isEqualToString:@""]) && !([bankid isEqualToString:@"0"])){
        urlstring = [NSString stringWithFormat:@"%@&bankid=%@",urlstring,bankid];
    }
    
    NSString *prodsStr = @"<products>";
    
    for (ProdModel *model in prods) {
        NSString *str= [NSString stringWithFormat:@"<product><id>%@</id><name>%@</name><amount>%@</amount></product>",[model pid], [model name], [model amount]];
        
        prodsStr = [NSString stringWithFormat:@"%@%@", prodsStr, str];
        
    }
    prodsStr = [NSString stringWithFormat:@"%@%@", prodsStr, @"</products>"];
    
    urlstring = [NSString stringWithFormat:@"%@&mprod=%@", urlstring, prodsStr];
    
    NSString *str = [NSString stringWithFormat:@"%@%@%@%@%@%@%@",loginid,password,@"NBFundTransfer",prodid,txnid,amt,@"INR"];
    
    NSString *signature = [self hmacSHA512:str withKey:reqHashKey];
    urlstring = [NSString stringWithFormat:@"%@&signature=%@",urlstring,signature];
    
    urlstring = [urlstring stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    // NSLog(@"Received Url------>%@",urlstring);
    
    //    NSURL *url = [NSURL URLWithString:urlstring];
    //    parser = [[NSXMLParser alloc] initWithContentsOfURL:url];
    
    NSError *error;
    NSURL *url = [NSURL URLWithString:urlstring];
    NSLog(@"URL----->%@",urlstring);
    
    
    NSURLRequest *requestObj = [NSURLRequest requestWithURL:url];
    
    self.viewWeb.scrollView.bounces = NO;
    [self.viewWeb loadRequest:requestObj];
    
  /*  NSString *dataString =[[NSString alloc]initWithContentsOfURL:url encoding:NSUTF8StringEncoding error:&error] ;
    
    NSLog(@"Error %@", error);
    
    NSLog(@"**datastring** %@", dataString);
    
    NSData *data  = [dataString dataUsingEncoding:NSUTF8StringEncoding];
    
    
    
    parser = [[NSXMLParser alloc] initWithData:data];
    
    [parser setDelegate:self];
    [parser setShouldResolveExternalEntities:NO];
    [parser parse];
   
    */
    
}

- (void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName attributes:(NSDictionary *)attributeDict {
    
    element = elementName;
    
    
    if([elementName isEqualToString:@"param"])
    {
        
        myAttrDict = [[NSDictionary alloc] initWithDictionary:attributeDict];
        [names addObject:[myAttrDict valueForKeyPath:@"name"]];
    }
    
    
    if([elementName isEqualToString:@"RESPONSE"])
    {
        names =[[NSMutableArray alloc]init];
        formUrl = [[NSMutableArray alloc] init];
        formParam = [[NSMutableArray alloc] init];
    }
    
    
}

- (void)parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName {
    
    
    
}

- (void)parser:(NSXMLParser *)parser foundCharacters:(NSString *)string {
    
    
    if([element isEqualToString:@"param"])
    {
        // NSLog(@"Adding String-->%@",string);
        [formParam addObject:string];
    }
    if([element isEqualToString:@"url"])
    {
        //  NSLog(@"Adding String-->%@",string);
        [formUrl addObject:string];
    }
    
    
}

- (void)parserDidEndDocument:(NSXMLParser *)parser1 {
    
    
    NSString *urlString = [formUrl objectAtIndex:0];
    urlString = [urlString stringByAppendingString:@"?"];
    NSString *type =[@"ttype=" stringByAppendingString:[formParam objectAtIndex:0]];
    NSString *tempTxnId =[@"tempTxnId=" stringByAppendingString:[formParam objectAtIndex:1]];
    NSString *token =[@"token=" stringByAppendingString:[formParam objectAtIndex:2]];
    NSString *txnStage =[@"txnStage=" stringByAppendingString:[formParam objectAtIndex:3]];
    
    //NSLog(@"urlString Url---->%@",urlString);
    
    NSString *encodedUrl =[urlString stringByAppendingString:type];
    encodedUrl = [encodedUrl stringByAppendingString:@"&"];
    encodedUrl = [encodedUrl stringByAppendingString:tempTxnId];
    encodedUrl = [encodedUrl stringByAppendingString:@"&"];
    encodedUrl = [encodedUrl stringByAppendingString:token];
    encodedUrl = [encodedUrl stringByAppendingString:@"&"];
    encodedUrl = [encodedUrl stringByAppendingString:txnStage];
   // NSLog(@"Encoded Url---->%@",encodedUrl);
    
    
    NSURL *url = [NSURL URLWithString:encodedUrl];
    
    NSURLRequest *requestObj = [NSURLRequest requestWithURL:url];
    viewWeb.scrollView.bounces =NO;
    [viewWeb loadRequest:requestObj];
}

-(void)webViewDidStartLoad:(UIWebView *)webView
{
    NSLog(@"Web View Did Start Load");
    //[self.viewWeb addSubview:activityIndicator];
    // [self.viewWeb addSubview:waitLabel];
    
    //    [activityIndicator startAnimating];
    [activityIndicator removeFromSuperview];
    [activityIndicator stopAnimating];
    [waitLabel removeFromSuperview];
    
}

-(BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType
{
    NSLog(@"should start load with request..");
      NSLog(@"Loading: %@", [request URL]);
    //    if ([request.URL.scheme isEqualToString:@"inapp"]) {
    //        if([request.URL.host isEqualToString:@"close"])
    //        {
    //
    //            [self.myDelegate secondviewcontrollerDissmissed:result];
    //            [self dismiss];
    //        }
    //    }
    //    return YES;
    
    if([request URL] != nil && ![[request URL] isEqual:@""]){
        return YES;
    } else {
        return NO;
    }
}
-(void)webViewDidFinishLoad:(UIWebView *)webView
{
    
      NSLog(@"webViewDidFinishLoad");
    
    
    NSLog(@"****didFinish: %@; ****stillLoading: %@", [[webView request]URL],
          (webView.loading?@"YES":@"NO"));
    
    [self.viewWeb stringByEvaluatingJavaScriptFromString:@"var div = document.createElement('div');div.innerHTML='<a href=\'https://www.atomtech.in/\' style= \'display: none;\'  ></a>';document.getElementsByTagName('body')[0].appendChild(div);"];
    //[viewWeb stringByEvaluatingJavaScriptFromString:@"window.alert=null;"];
    [activityIndicator removeFromSuperview];
    [activityIndicator stopAnimating];
    [waitLabel removeFromSuperview];
    // [viewWeb stringByEvaluatingJavaScriptFromString:@"window.alert=null;"];
    
    NSString *currentURL = viewWeb.request.URL.absoluteString;
    
    if([currentURL rangeOfString:@"atomtech.in"].location != NSNotFound)
    {
        imageView.hidden = YES;
    } else {
        imageView.hidden = NO;
    }
    
    //if([currentURL rangeOfString:@"param"].location != NSNotFound)
    if([currentURL hasSuffix:@"/mobilesdk/param"])

    {
        NSString *htmlSource = [self.viewWeb stringByEvaluatingJavaScriptFromString:@"document.documentElement.outerHTML"];
        // NSLog(@"html---->%@",htmlSource);
        
        
        /* if([htmlSource rangeOfString:@"success_00" options:NSCaseInsensitiveSearch].location != NSNotFound)
         {
         result = @"Transaction Successfull!";
         // NSLog(@"Transaction Successfull!");
         
         }
         else
         {
         result = @"Transaction Failed!";
         //  NSLog(@"Transaction Failed!");
         
         }*/
        
        if([htmlSource rangeOfString:@"success_00" options:NSCaseInsensitiveSearch].location != NSNotFound)
        {
            
            
            //result = @"Transaction Successfull!";
            result=[self convertHTMLTableString:htmlSource];
            NSLog(@"Transaction Successfull!");
            //result = htmlSource;
        }
        else
        {
            //result = @"Transaction Failed!";
            result = [self convertHTMLTableString:htmlSource];
             NSLog(@"Transaction Failed!");
            //result=htmlSource;
        }
        
        //  if([self.myDelegate respondsToSelector:@selector(secondviewcontrollerDissmissed:)])
        //  {
        
    
        
        
        
        //  }
        
        [self performSelector:@selector(dismiss) withObject:nil afterDelay:5.0];
        
        
    }
    
}

- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error {
    NSLog(@"didFail: %@; stillLoading: %@", [[webView request]URL],
          (webView.loading?@"YES":@"NO"));
}




-(void)dismiss
{
    [self dismissViewControllerAnimated:YES completion:nil];
    
    [self.myDelegate secondviewcontrollerDissmissed:result];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



- (void)alertView:(UIAlertView *)alert clickedButtonAtIndex:(NSInteger)buttonIndex {
    
    if (buttonIndex == alert.cancelButtonIndex) {
        [self viewDidAppear:YES];
    }
}




-(NSString *)convertHTMLTableString:(NSString *)htmlString{
    
    //    NSString *formattedStr;
 /*   htmlString=[self removeHeaderTag:htmlString];
    htmlString=[self validateTableRow:htmlString];
    htmlString=[self validateTableData:htmlString];
    htmlString=[self removeHeaderEnd:htmlString];
    
    
    htmlString=[[[[[[[[[htmlString stringByReplacingOccurrencesOfString:@"<tr>" withString:@"|"]stringByReplacingOccurrencesOfString:@"</tr>" withString:@""]stringByReplacingOccurrencesOfString:@"</td><td>" withString:@"="]stringByReplacingOccurrencesOfString:@"<td></td>" withString:@"="]stringByReplacingOccurrencesOfString:@"<td>" withString:@""]stringByReplacingOccurrencesOfString:@"</td>" withString:@""]stringByReplacingOccurrencesOfString:@"<ul>" withString:@""]stringByReplacingOccurrencesOfString:@"</ul>" withString:@""]stringByReplacingOccurrencesOfString:@"\n" withString:@""];
    
    NSLog(@" formatted HTML String ==> %@",htmlString);
    
    return htmlString;*/
    
    NSString *result = @"";
    if(htmlString != nil){
        NSArray *splitWithH5EndTag = [htmlString componentsSeparatedByString:@"</h5>"];
        if(splitWithH5EndTag != nil && [splitWithH5EndTag count] > 1){
            NSString *resString = splitWithH5EndTag[0];
            NSArray *dataArray = [resString componentsSeparatedByString:@"<h5 style=\"color:#ffffff\">"];
            if(dataArray != nil && [dataArray count] > 1){
                result = dataArray[1];
            }
        }
    }
    if([result isEqualToString:@""]){
        result = @"Transaction Failed";
    }
    NSLog(@" formatted HTML String ==> %@",result);
    return result;
}

-(NSString *)removeHeaderTag:(NSString *)htmlString{
    
    NSString *endOfHeader=@"<tr";
    NSString *headerStr;
    
    NSRange endRange=[htmlString rangeOfString:endOfHeader];
    if(endRange.location!=NSNotFound){
        NSRange targetRange;
        targetRange.location=0;
        targetRange.length=targetRange.location+endRange.location;
        //        NSLog(@"target length= %lu",(unsigned long)targetRange.length);
        headerStr=[htmlString substringWithRange:targetRange];
        //        NSLog(@"Header string %@",headerStr);
        htmlString=[htmlString stringByReplacingOccurrencesOfString:headerStr withString:@""];
    }
    
    htmlString = [htmlString componentsSeparatedByString:@"</th></tr>"][1];
    
    
    return htmlString;
}

-(NSString *)removeHeaderEnd:(NSString *)htmlString{
    
    NSString *startHeaderEnd=@"</tr></";
    NSString *headerStr;
    
    NSRange endRange=NSMakeRange(0,[htmlString length]);
    if(endRange.location!=NSNotFound){
        NSRange targetRange;
        targetRange.location=[htmlString rangeOfString:startHeaderEnd].location;
        targetRange.length=[htmlString length]-targetRange.location;
        //         NSLog(@"target length= %lu",(unsigned long)targetRange.length);
        headerStr=[htmlString substringWithRange:targetRange];
        //        NSLog(@"Header string %@",headerStr);
        htmlString=[htmlString stringByReplacingOccurrencesOfString:headerStr withString:@""];
    }
    htmlString=[htmlString stringByAppendingString:@"</tr>"];
    return htmlString;
    
}

-(NSString *)validateTableRow:(NSString *)htmlString{
    
    NSString *startRowTag=@"<tr";
    NSString *endRowTag=@">";
    NSString *substringRow;
    
    NSRange startRange=[htmlString rangeOfString:startRowTag];
    
    if (startRange.location != NSNotFound) {
        NSRange targetRange;
        targetRange.location = startRange.location + startRange.length;
        targetRange.length = [htmlString length] - targetRange.location;
        NSRange endRange = [htmlString rangeOfString:endRowTag options:0 range:targetRange];
        if (endRange.location != NSNotFound) {
            targetRange.length = endRange.location - targetRange.location;
            //            NSLog(@"%@", [htmlString substringWithRange:targetRange]);
            substringRow=[htmlString substringWithRange:targetRange];
            htmlString=[htmlString stringByReplacingOccurrencesOfString:substringRow withString:@""];
        }
    }
    return htmlString;
}

-(NSString *)validateTableData:(NSString *)htmlString{
    
    NSString *startDataTag=@"<td";
    NSString *endDataTag=@">";
    NSString *substringData;
    
    NSRange startRange=[htmlString rangeOfString:startDataTag];
    
    if (startRange.location != NSNotFound) {
        NSRange targetRange;
        targetRange.location = startRange.location + startRange.length;
        targetRange.length = [htmlString length] - targetRange.location;
        NSRange endRange = [htmlString rangeOfString:endDataTag options:0 range:targetRange];
        if (endRange.location != NSNotFound) {
            targetRange.length = endRange.location - targetRange.location;
            //            NSLog(@"%@", [htmlString substringWithRange:targetRange]);
            substringData=[htmlString substringWithRange:targetRange];
            htmlString=[htmlString stringByReplacingOccurrencesOfString:substringData withString:@""];
        }
    }
    return htmlString;
}

- (IBAction)cancelNav:(id)sender
{
    result = @"Transaction Failed!";
    // NSLog(@"Transaction Failed!");
    [self.myDelegate secondviewcontrollerDissmissed:result];
    [self dismissViewControllerAnimated:YES completion:nil];
    
}
-(NSString*)hmacSHA512:(NSString*)dataStr withKey:(NSString *)keyStr {
    
    NSData *key = [keyStr dataUsingEncoding:NSUTF8StringEncoding];
    NSData *dataIn =[dataStr dataUsingEncoding:NSUTF8StringEncoding];
    
    
    NSMutableData *macOut = [NSMutableData dataWithLength:CC_SHA512_DIGEST_LENGTH];
    
    CCHmac(kCCHmacAlgSHA512,
           key.bytes,
           key.length,
           dataIn.bytes,
           dataIn.length,
           macOut.mutableBytes);
    NSString * newStr = [[NSString alloc] initWithData:macOut encoding:NSASCIIStringEncoding];
    
    //NSLog(@"strnew%@  macOut%@",newStr,macOut);
    
    
    return [self hexadecimalString:(macOut)];
}

- (NSString *)hexadecimalString: (NSMutableData*)data {
    
    
    //    NSUInteger len = [data length];
    //    Byte *byteData = (Byte*)malloc(len);
    //    memcpy(byteData, [data bytes], len);
    //
    const unsigned char *dataBuffer = data.bytes;
    if (!dataBuffer)
        return [NSString string];
    
    NSUInteger dataLength = data.length;
    NSMutableString *hexString = [NSMutableString stringWithCapacity:(dataLength *2)];
    
    for (int i = 0; i < dataLength; ++i)
        [hexString appendString:[NSString stringWithFormat:@"%02lx", (unsigned long)dataBuffer[i]]];
    
    return [NSString stringWithString:hexString];
}


-(NSString*)base64forData:(NSData*)theData {
    const uint8_t* input = (const uint8_t*)[theData bytes];
    NSInteger length = [theData length];
    
    static char table[] = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=";
    
    NSMutableData* data = [NSMutableData dataWithLength:((length + 2) / 3) * 4];
    uint8_t* output = (uint8_t*)data.mutableBytes;
    
    NSInteger i;
    for (i=0; i < length; i += 3) {
        NSInteger value = 0;
        NSInteger j;
        for (j = i; j < (i + 3); j++) {
            value <<= 8;
            
            if (j < length) {  value |= (0xFF & input[j]);  }  }  NSInteger theIndex = (i / 3) * 4;  output[theIndex + 0] = table[(value >> 18) & 0x3F];
        output[theIndex + 1] = table[(value >> 12) & 0x3F];
        output[theIndex + 2] = (i + 1) < length ? table[(value >> 6) & 0x3F] : '=';
        output[theIndex + 3] = (i + 2) < length ? table[(value >> 0) & 0x3F] : '=';
    }
    
    return [[NSString alloc] initWithData:data encoding:NSASCIIStringEncoding];
}


@end





