
import UIKit

class ViewController: UIViewController, XMLParserDelegate, nbDelegate, cardDelegate {
    
    
    @IBOutlet weak var initiateRequest: UIButton!
    var parser: XMLParser?
    
    
    @IBAction func callVCard(_ sender: Any) {
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    @IBAction func callVC(_ sender: Any) {
        
        var prods: [ProdModel] = []
        
        let prod1 = ProdModel()
        prod1.pid = "1"
        prod1.name = "One"
        prod1.amount = "100.00"
        
        let prod2 = ProdModel()
        prod2.pid = "2"
        prod2.name = "Two"
        prod2.amount = "100.00"
        
        
        prods.append(prod1)
        prods.append(prod2)
        
        
        let netB = nb()
        netB.myDelegate = self
        netB.txnscamt = "0"
        netB.loginid = "2"
        netB.password = "NCA@1234"
        netB.txncurr = "INR"
        netB.clientcode = "001"
        netB.custacc = "100000036600"
        netB.amt = "200.000"
        netB.txnid = "MID123456790"
        netB.date = "20/05/2020 12:16:39"
        netB.reqHashKey = "KEYeea184b57cb333adab"
        netB.resHashKey = "KEY137f4d7c388641a901"
        netB.prods = ((prods as NSArray).mutableCopy() as! NSMutableArray)
        netB.prodid = "Multi"
        netB.merchantId = "1191";
        //netB.bankid=@"2001";
        
       /* let netB = nb()  //for atom
        netB.myDelegate = self
        netB.txnscamt = "0"
        netB.loginid = "2"
        netB.password = "Test@123"
        netB.txncurr = "INR"
        netB.clientcode = "001"
        netB.custacc = "100000036600"
        netB.amt = "200.000"
        netB.txnid = "MID123456790"
        netB.date = "20/05/2020 12:16:39"
        netB.reqHashKey = "KEY123657234"
        netB.resHashKey = "KEYRESP123657234"
        netB.prods = ((prods as NSArray).mutableCopy() as! NSMutableArray)
        netB.prodid = "Multi"
        //netB.merchantId=@"2";
        //netB.bankid=@"2001";
       */
        
        present(netB, animated: true)

        
    }
    
    func secondviewcontrollerDissmissed(_ stringToFirst: String?) {
        var getResult: String
        getResult = stringToFirst ?? ""
        print("multi mid received response---->\(getResult)")
    }
}



